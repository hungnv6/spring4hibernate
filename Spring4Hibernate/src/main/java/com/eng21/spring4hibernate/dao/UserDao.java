package com.eng21.spring4hibernate.dao;

import java.util.List;

import com.eng21.spring4hibernate.model.User;

 
public interface UserDao {
 
    User findById(int id);
     
    User findBySSO(String sso);
     
    void save(User user);
     
    void deleteBySSO(String sso);
     
    List<User> findAllUsers();
 
}
