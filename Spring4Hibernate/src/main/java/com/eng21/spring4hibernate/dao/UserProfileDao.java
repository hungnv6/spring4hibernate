package com.eng21.spring4hibernate.dao;

import java.util.List;

import com.eng21.spring4hibernate.model.UserProfile;

public interface UserProfileDao {
	List<UserProfile> findAll();
    
    UserProfile findByType(String type);
     
    UserProfile findById(int id);
}
