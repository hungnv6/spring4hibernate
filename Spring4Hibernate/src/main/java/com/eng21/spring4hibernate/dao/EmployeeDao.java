package com.eng21.spring4hibernate.dao;

import java.util.List;

import com.eng21.spring4hibernate.model.Employee;

public interface EmployeeDao {
	 
    void saveEmployee(Employee employee);
     
    List<Employee> findAllEmployees();
     
    void deleteEmployeeBySsn(String ssn);
     
    Employee findBySsn(String ssn);
     
    void updateEmployee(Employee employee);
}
