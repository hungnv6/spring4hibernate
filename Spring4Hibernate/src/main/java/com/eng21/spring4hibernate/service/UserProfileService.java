package com.eng21.spring4hibernate.service;

import java.util.List;

import com.eng21.spring4hibernate.model.UserProfile;

public interface UserProfileService {

	UserProfile findById(int id);
	 
    UserProfile findByType(String type);
     
    List<UserProfile> findAll();
}
