package com.eng21.spring4hibernate.daoImpl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.eng21.spring4hibernate.dao.AbstractDao;
import com.eng21.spring4hibernate.dao.UserProfileDao;
import com.eng21.spring4hibernate.model.UserProfile;


@Repository("userProfileDao")
public class UserProfileDaoImpl extends AbstractDao<Serializable, UserProfile> implements UserProfileDao {

	public UserProfile findById(int id) {
        return getByKey(id);
    }
 
    public UserProfile findByType(String type) {
        Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("type", type));
        return (UserProfile) crit.uniqueResult();
    }
     
    @SuppressWarnings("unchecked")
	public List<UserProfile> findAll(){
//        Criteria crit = createEntityCriteria();
//        crit.addOrder(Order.asc("type"));
//        return (List<UserProfile>)crit.list();
    	Criteria criteria = getSession().createCriteria(UserProfile.class);
    	criteria.addOrder(Order.asc("type"));
        return (List<UserProfile>) criteria.list();
    }
}
